import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';


import { ProfileComponent } from './profile/profile.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { AdduserComponent } from './adduser/adduser.component';

const routes: Routes = [
  { path: 'home', component: LandingComponent },
  { path: 'profile', component: ProfileComponent },
  //{ path: 'register', component: SignupComponent },
  //{ path: 'landing', component: LandingComponent },
  { path: 'add-user', component: AdduserComponent },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
