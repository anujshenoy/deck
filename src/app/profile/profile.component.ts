import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { AdduserComponent } from '../adduser/adduser.component';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
    
   

    constructor(public authService: AuthService, private dialog: MatDialog) { }

    ngOnInit() {}

    addUserModal() {
        this.authService.initAddUserFormGroup();
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.width = '80%';
        dialogConfig.height = '80%';
        dialogConfig.data = {
          'method': 'addUser'
        };
        const ref = this.dialog.open(AdduserComponent, dialogConfig);
        ref.afterClosed().subscribe(result => {
            console.log(result)
          
        });
    
      }

}
