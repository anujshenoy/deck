import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { NotificationService } from '../shared/notification.service';
@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',

})
export class AdduserComponent implements OnInit {

  constructor(public authService: AuthService, private notify: NotificationService) { }

  ngOnInit(): void {
  }

  addUser() {
    if (this.authService.addUserForm.valid) {
      this.authService.addUserForm.controls['firstName'].enable();
      this.authService.addUserForm.controls['lastName'].enable();
      this.authService.addUserForm.controls['emailId'].enable();
      this.authService.addUserForm.controls['city'].enable();
      this.authService.addUserForm.controls['state'].enable();
      this.authService.addUserForm.controls['country'].enable();
      this.authService.addUserForm.controls['password'].enable();
      this.authService.addUserForm.controls['adminRoleType'].enable();
      this.authService.addUserForm.controls['dateOfBirth'].enable();


      this.authService.addUser(this.authService.addUserForm.value).subscribe(
        response => {
          if (response.responseStatus === 200) {
            this.authService.addUserForm.reset();
            this.authService.initAddUserFormGroup();
            this.notify.success('User Added Successfully');
          }
        },

      );

      this.onClose();
    }
  }

  onClose() {
    this.authService.addUserForm.reset();
    this.authService.initAddUserFormGroup();
  }

}
