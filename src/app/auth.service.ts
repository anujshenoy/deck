import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})


export class AuthService {

    private getUrl = "https://pharmadeck.herokuapp.com/api/user/getUsers/";
    private addUrl = "https://pharmadeck.herokuapp.com/api/user/addUser/";
    private loginUrl = "http://pharmadeck.herokuapp.com/api/user/Login";

    constructor(private http: HttpClient) { }


    addUserForm: FormGroup = new FormGroup({
        firstName: new FormControl('', [
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(20),
            Validators.pattern(/^[a-zA-z]+$/)
        ]),

        lastName: new FormControl('', [
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(20),
            Validators.pattern(/^[a-zA-z]+$/)
        ]),

        emailId: new FormControl('', [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern(/^([0-9a-zA-Z]([.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)[0-9a-zA-Z\.]{1,3}[\w-]{1,2})$/)
        ]),

        city: new FormControl('', [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
        ]),

        state: new FormControl('', [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
            Validators.pattern(/^[a-zA-Z ]+$/)
        ]),

        country: new FormControl('', [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
        ]),

        adminRoleType: new FormControl('', [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
            Validators.pattern(/^[a-zA-Z0-9 ]+$/)
        ]),

        password: new FormControl('', [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
        ]),

        dateOfBirth: new FormControl('', [Validators.required]),
        

    })

    initAddUserFormGroup() {
        this.addUserForm.setValue({
            firstName: '',
            lastName: '',
            emailId: '',
            city: '',
            state: '',
            country: '',
            password: '',
            adminRoleType: '',
            dateOfBirth: '',
        });
    }

    addUser(user) {
        console.log(user);
        const str = {
            'firstName': user.firstName,
            'lastName': user.lastName,
            'emailId': user.emailId,
            'city': user.city,
            'state': user.state,
            'country': user.country,
            'password': user.password,
            'adminRoleType': user.adminRoleType,
            'dateOfBirth': user.dateOfBirth,
        }
        return this.http.post<any>(this.addUrl, JSON.stringify(str));
    }

    getUser(user): Observable<any> {
        return this.http.post<any>(this.getUrl, user);
    }

    loginUser(user): Observable<any> {
        return this.http.post<any>(this.loginUrl, user)
    }
}