import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar'

@Injectable({
  providedIn: 'root',
})
export class NotificationService {

  constructor(private snackBar: MatSnackBar) { }

  snackBarConfig: MatSnackBarConfig = {
    duration: 4000,
    horizontalPosition: 'center',
    verticalPosition: 'bottom',
    panelClass: ['standard-snackbar']
  };

  success(msg) {
    console.log(msg);
    if (msg === '' || msg === undefined || msg === null) {
      return;
    }
    this.snackBar.open(msg, '', this.snackBarConfig);
  }

  error(msg) {
    if (msg === '' || msg === undefined || msg === null) {
      return;
    }
    this.snackBar.open(msg, '', {
      duration: 4000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['standard-snackbar']
    });
  }

  warning(msg) {
    this.snackBar.open(msg, '', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['blue-snackbar']
    });
  }

  infomation(msg) {
    this.snackBar.open(msg, '', {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['yellows-snackbar']
    });
  }
}
